//Homepage
let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
let baseUrl = "http://localhost:3000/";
const doLikes = document.querySelector(".doLike");
const doDislikes = document.querySelector(".doDislike");
const params = window.location.search;
const user_id = new URLSearchParams(params).get('user');
// document.getElementById("channelId").href = "http://localhost:3000/api/v1/video/channel/" + user_id;

//Subsribers button
const doSubscribes = document.querySelector(".doSubscribes");

//comment variables
const commentVideoForm = document.querySelector(".postComment");
const commentValue = document.querySelector(".comment");

//notifications variables
const dropDownNotif = document.getElementById("dropDownMenuButton");

//Save history
const saveHistory = document.querySelector(".saveHistory");

//delete video histories
const deleteHistory = document.querySelector(".deleteHistory");

//Get User Id
const getUserId = document.querySelector(".getUserId");
console.log(getUserId);
getUserId.addEventListener("click", (e) => {
	e.preventDefault();
	axios.get("/api/v1/video/get-user-id")
		 .then((response) => {
		 	if(response.request.readyState == 4 && response.data.status == "success") {
		 		const channelId = document.getElementById("channelId").href = `http://localhost:3000/api/v1/video/channel/${response.data.userId}`;
		 		window.location.href = `http://localhost:3000/api/v1/video/channel/${response.data.userId}`;
		 	}else{
		 		alert(response.data.message)
		 	}
		 	console.log(response);
		 })
});

//Show all notifications
dropDownNotif.addEventListener("click", async(e) => {
	const data = await axios.get(`/api/v1/video/get-notif`).then((response)=>{
		if(response.request.readyState == 4 && response.data.status == "success") {
			let responseText = JSON.parse(response.request.responseText);

			if(response.data.status == "success") {
				data_user = response.data.user;

				html = "";

				notifications = data_user.notifications.reverse();
				for(let a = 0; a < notifications.length; a++) {
					let notification = notifications[a];
					if(!notification.is_read) {
						if(notification.notif_type == "new comment") {
							html += `<a href="/api/v1/video/watch/${notification.video_watch}" class="dropdown-item" onclick="readNotification(this);" data-id="${notification._id}">New comment</a>`
						}else if(notification.notif_type == "new_reply") {
							html += `<a href="/api/v1/video/watch/${notification.video_watch}" class="dropdown-item" onclick="readNotification(this);" data-id="${notification._id}">New Reply</a>`			
						}
					}
				}
				document.getElementById("unread-notifications").innerHTML = html;
			}else{
				alert(response.data.message);
			}
		}
	})
})

async function readNotification(self) {
	let id = self.getAttribute("data-id");
	await axios.patch(`/api/v1/video/get-notif`, { _id: id }).then((response) => {
		if(response.request.readyState == 4 && response.data.status == "success") {
			let responseText = JSON.parse(response.request.responseText);
			if(response.data.status == "error") {
				alert(response.data.message);
			}
		}
	})
	// .then((response)=> {
	// 	// console.log(response.notifications);
	// 	console.log(response);
	// 	var object = JSON.parse(response);
	// 	console.log(object["notifications"]); 
	// })
}

function createReplyNode(node) {
	let commentId = node.getAttribute("data-comment-id");
	let html = "";

	html += `
		<div class="row">
			<div class="col-md-12">
				<form class="postReply">
					<input type="hidden" name="commentId" value="${commentId}" class="commentId">
					<div class="form-group">
						<label>Write Reply</label>
						<textarea class="form-control textarea-input" name="reply"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Post</button>
				</form>
			</div>
		</div>
	`

	node.innerHTML = html;
	node.removeAttribute("onclick")

const postReply = document.querySelector(".postReply");
postReply.addEventListener("submit", async (e) => {
	e.preventDefault();
	let commendId_onReply = postReply.firstElementChild.value;
	let textareaInput = document.querySelector(".textarea-input");

	try {
		axios.post(`/api/v1/video/do-comment`, {
			_id: commentId,
			replyBody: textareaInput.value
		}).then((response) => {
			console.log(response);
			if(response.request.readyState == 4 && response.data.status == "success") {
				let responseText = JSON.parse(response.request.responseText);

				if(response.data.status == "success") {
					const userData = response.data;
					let replyBodies = "";
					replyBodies += `
						<div class="media mt-4">
							<img src="${userData.user.image}" class="d-flex mr-3 comment-img" onerror="this.src='http://placehold.it/100x100'">
							<div class="media-body">
								<h5 class="mt-0">${userData.user.name}</h5>
								${textareaInput.value}
							</div>
						</div>
					`
					document.getElementById("replies").innerHTML += replyBodies;
					textareaInput.value = "";
					return false;
				}
			}else{
				alert(response.data.message);
			}
		})
	}catch (error) {
		console.log(error);
	}

});
}

//Do likes
doLikes.addEventListener("click", async (e) => {
	e.preventDefault();

	try {
		await axios.patch(`/api/v1/video/do-likes/${videoId}`, {
			_id: videoId
		}).then((response) => {
			// console.log(response.data);
			console.log(response)
			if(response.request.readyState == 4 && response.data.status == "success") {
				//Update likes
				let countLikes = document.getElementById("likes").innerHTML;
				countLikes = parseInt(countLikes);
				countLikes++;
				document.getElementById("likes").innerHTML = countLikes;
			}else{
				alert(response.data.message);
			}
		})

	} catch (error) {
		console.log(error);
	}
});

//Do Dislike
doDislikes.addEventListener("click", async (e) => {
	e.preventDefault();

	try {
		await axios.patch(`/api/v1/video/do-disLikes/${videoId}`, {
			_id: videoId
		}).then((response) => {
			// console.log(response.data);
			console.log(response)
			if(response.request.readyState == 4 && response.data.status == "success") {
				//Update likes
				let countDislikes = document.getElementById("dislikes").innerHTML;
				countDislikes = parseInt(countDislikes);
				countDislikes++;
				document.getElementById("dislikes").innerHTML = countDislikes;
			}else{
				alert(response.data.message);
			}
		})

	} catch (error) {
		console.log(error);
	}
});

let videoId = document.getElementById("videoId").value;
//Video comments
commentVideoForm.addEventListener("submit", async (e) => {
	e.preventDefault(e);

	try {
		let comment = commentValue.value;

		let data  = await axios.post(`/api/v1/video/do-comment/${videoId}`, {
			_id: videoId,
			comment: comment
		});
		console.log(data);
		const userID = data.data.user._id; 
		const username = data.data.user.name; 
		const user_image = data.data.user.image; 

		let html = "";
		html += `
			<div class="media mb-4">
				<img class="d-flex mr-3 comment-img" src="${user_image}" onerror="this.src = 'http://placehold.it/100x100'">

				<div class="media-body">
					<h5 class="mt-0">${username}</h5>
					${comment}
				</div>
			</div>
		`
		document.getElementById("comments").innerHTML = html + document.getElementById("comments").innerHTML;
		commentValue.value = "";

	} catch(error) {
		console.log(error)
	}
});


//Subscribe
doSubscribes.addEventListener("click", (e) => {
	e.preventDefault();
	const videoId = document.getElementById("videoId").value;
	axios.post(`/api/v1/video/do-subscribe/`, {
		_id: videoId
	}).then((response)=> {
			if(response.request.readyState == 4 && response.data.status == "success") {
				let responseText = JSON.parse(response.request.responseText);
				if(response.data.status == "success") {
					let totalSubscribers = parseInt(document.getElementById("total-subscribers").innerHTML);
					totalSubscribers++;
					document.getElementById("total-subscribers").innerHTML = totalSubscribers;
				}
			}else{
				alert(response.data.message);
			}
		})
})

//Show related Videos
const showRelatedVideos = async () => {
	let category = document.getElementById("category").lastChild.parentNode.innerText;
	category = category.replace("Category: ", "").trim();
	//textContent.trim();
	console.log(category);
	const videoId = document.getElementById("videoId").value;
	try{
		await axios.get(`/api/v1/video/get-related-videos/`, {
			params: {
				category: category,
				_id: videoId
			}
		}).then((response) => {
			if(response.request.readyState == 4 && response.data.status == "success") {
				const videos = response.data.data;
				let html = "";
				for (let i = 0; i < videos.length; i++) {
					let video = videos[i];
					let flag = false;

					let createdAt = new Date(video.createdAt);
					let date = createdAt.getDate() + "";
					// date = date.padStart(2, "0") + " " + months[createdAt.createdAt.getMonth()] + ", " + createdAt.getFullYear();
					html += `
						<div>
							<img src="${baseUrl}${video.thumbnail}" class="img-fluid">
							<p>${video.minutes}:${video.seconds}</p>
							<h3><a href="/api/v1/video/watch/${video.watch}">${video.title}</a></h3>
						</div>
					`
				}
				document.getElementById("related-videos").innerHTML = html;
			}else{
				alert(response.data.message)
			}
		});
	} catch(error) {

	}
}

showRelatedVideos();

//Save my history
// saveHistory.addEventListener("load", async (e) => {
// 	e.preventDefault();
// 	const videoPlayer = document.getElementById("videoPlayer");
// 	console.log(videoPlayer);
// 	const videoId = document.getElementById("videoId").value;
// 	let watched = Math.floor(videoPlayer.currentTime);

// 	if(watched > 0) {
// 		try {
// 			await axios.post(`/api/v1/video/save-history`, {
// 				params: {
// 					videoId: videoId,
// 					watchedTime: watched
// 				}
// 			}).then((response) => {
// 				if(response.request.readyState == 4 && response.data.status == "success") {
// 					console.log(response.request.responseText);
// 				}else{
// 					alert(response.data.message);
// 				}
// 			})
// 		}catch (error) {
// 			console.log(error);
// 		}
// 	}
// 	e.returnValue = "";
// });

//Delete History
deleteHistory.addEventListener("submit", async (e) => {
	e.preventDefault();
	console.log(this);
	const historyId = document.querySelector(".deleteBtnID");;
	
	try {
		axios.post(`/api/v1/video/delete-history`, {
			historyId: historyId.value
		}).then((response) => {
			if(response.request.readyState == 4 && response.data.status == "success") {
				// console.log(response);
				const videoHistoryContainer = historyId.parentElement.parentElement.parentElement;
				videoHistoryContainer.remove();
				let result = ""
			}else{
				alert(response.data.message);
			}
		})

	} catch(error) {
		console.log(error);
	}

})