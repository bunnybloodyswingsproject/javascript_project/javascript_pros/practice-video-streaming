require("dotenv").config();
require("express-async-errors"); //Use to deliver the proper response you want in the request

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const expressSession = require("express-session");

//connecting database...
const connectDB = require("./db/connect");
const authenticateUser = require("./middleware/authentication");
const authRouter = require("./routes/auth");
const videoRouter = require("./routes/video");
const Videos = require("./models/Videos");
const User = require("./models/User");

//Error Handler
const notFoundMiddleware = require("./middleware/not-found");
const errorHandlerMiddleware = require("./middleware/error-handler");

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true,
	limit: "10000mb",
	parameterLimit: 1000000
}))

app.use(expressSession ({
	"key": "user_id",
	"secret": "User secret ID only",
	"resave": true,
	"saveUninitialized": true
}));


app.use("/public", express.static(__dirname + "/public"));
app.set("view engine", "ejs");

// app.get("/", async (req, res) => {
// 	await Videos.find({})
// 		.sort({"createdAt": -1})
// 		.toArray(function (error, videos) {
// 			res.render("index", {
// 				"isLogin": req.session.user_id ? true : false,
// 				"videos": videos
// 		});
// 	})
// });

app.get("/", (req, res) => {

	 Videos.find({})
		.sort({"createdAt": -1})
		.exec((err, videos) => {
			if(err) {
				console.log(err)
			}else{
				res.render("index", {
					"isLogin": req.session.user_id ? true : false,
					"videos": videos
				})
			}
		})
});
//Routers
app.use("/api/v1/auth", authRouter);
app.use("/api/v1/video", videoRouter);

app.use(errorHandlerMiddleware);
app.use(notFoundMiddleware);

const PORT = process.env.PORT;
const start = async () => {
	try {
		await connectDB(process.env.MONGO_URI);
		app.listen(PORT, () => {
			console.log(`Server is listening on port ${PORT}`);
		});
	} catch(error) {
		console.log(error);
	}
}

start();