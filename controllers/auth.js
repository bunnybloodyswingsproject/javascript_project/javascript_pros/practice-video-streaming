const User = require("../models/User");
const {StatusCodes} = require("http-status-codes");
const {BadRequestError, UnauthenticatedError} = require("../errors");
const session = require("express-session");
//Get Register page and post a user
const renderRegisterPage = async (req, res) => {
	res.render("signup");
}

const createUser = async (req, res) => {
	const user = await User.create({...req.body});
	const token = user.createJWT();
	console.log(token);
	res
		.status(StatusCodes.CREATED)
		// .send({user: {name: user.name}, token})
		.redirect("/api/v1/auth/login")
}

//Get Login page and log a user
const renderLogin = async (req, res) => {
	res.render("login", {
		"error": "",
		"message": ""
	});
}

//Get Login page and log a user
const userLogin = async (req, res) => {
	// res.render("login");
	const {email, password} = req.body;

	if(!email || !password) {
		throw new BadRequestError(`Please provide email and password`);
	}
	const user = await User.findOne({email});
	if(!user) {
		throw new UnauthenticatedError(`Email not found`);
	}

	const isPasswordCorrect = await user.comparePassword(password);
	if(!isPasswordCorrect) {
		throw new UnauthenticatedError(`Password didn't match the data in the system`)
	}

	const token = user.createJWT();
	req.session.user_id = user._id;
	res
		.status(StatusCodes.OK)
		// .json({ user: {name: user.name}, token })
		.redirect("/");
}

const logout = async (req, res) => {
	req.session.destroy();
	res.redirect("/");
}

module.exports = {
	renderRegisterPage,
	createUser,
	renderLogin,
	userLogin,
	logout
}