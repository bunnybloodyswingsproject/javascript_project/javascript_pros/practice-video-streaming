const User = require("../models/User");
const Video = require("../models/Videos");
const {StatusCodes} = require("http-status-codes");
const {BadRequestError, UnauthenticatedError} = require("../errors");
const session = require("express-session");
const formidable = require("formidable");
const path = require("path");
const multer = require("multer");
const fileSystem = require("fs");

const mongoose = require("mongoose");
let newId = new mongoose.Types.ObjectId();

const storage = multer.diskStorage({
  //Setting destination of uploading files	
  destination: function (req, file, cb) {
  	//if uploading videos
  	if(file.fieldname === "uploadedVideo") {
  		cb(null, './public/videos');
  	}else if(file.fieldname === "thumbnail"){
  		cb(null, "./public/thumbnails");
  	}else{
  		cb(null, "./public/thumbnails");
  	}
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + file.originalname;
    cb(null, file.fieldname + '-' + uniqueSuffix)
  }
});

const upload = multer({ 
	storage: storage,
	limits: {fileSize: function(req, file, cb) {
		checkFileSize(file, cb);
	}},
	fileFilter: function(req, file, cb) {
		checkFileType(file, cb);
	}
}).fields([
	{
		name: "uploadedVideo",
		maxCount: 1
	},
	{
		name: "thumbnail",
		maxCount: 1
	},
	{
		name: "profilePicture",
		maxCount: 1
	},
	{
		name: "coverPicture",
		maxCount: 1
	}
]);

//Check files filesize
function checkFileSize(file, cb) {
	if(file.fieldname === "uploadedVideo") {
		return 1048576000;
	}else{
		return 10000000;
	}
}

// Check file type
function checkFileType(file, cb) {
	if(file.fieldname === "uploadedVideo") {
		//Allowed extension
		const filetypes = /mp4|gif/;
		//Check ext
		const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
		//Check mime
		const mimetype = filetypes.test(file.mimetype);

		if(mimetype && extname) {
			return cb(null, true);
		}else{
			return cb("Error: Video field should be images files Only!")
		}		
	}else{
		//Allowed extension
		const filetypes = /jpeg|jpg|png|gif/;
		//Check ext
		const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
		//Check mime
		const mimetype = filetypes.test(file.mimetype);

		if(mimetype && extname) {
			return cb(null, true);
		}else{
			return cb("Error: Thumbnail field should be images files Only!")
		}
	}
}

const fs = require("fs");
const { getVideoDurationInSeconds } = require("get-video-duration");
//function to return user'sdocument
const getUser =  (id, callback) => {
	try {
		const user = User.findOne({_id: id}, function (error, result) {
			callback(result)
		});
		if(!user) {
			throw new UnauthenticatedError("Invalid User Id"); 
		}
	} catch (err) {
		console.log(err);
		return err;
	}
}

const usersChannel = async (req, res) => {
	getUser(req.params.user_id, function (user) {
		if(user == null) {
			res.send("Channel not found");
		}else{
			res.render("single-channel", {
				"isLogin": req.session.user_id ? true : false,
				user: user,
				isMyChannel: req.session.user_id == req.params.user_id
			})
		}
	})
}

const uploadVideos = async (req, res) => {
	if (req.session.user_id) {
		res.render("upload",{
			"isLogin": true
		})
	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const createVideo = async (req, res) => {
	//Check if user is logged in
	if(req.session.user_id) {
		upload(req, res, (err) => {
			if(err) {
				res.render("upload", {
					msg: err
				});
			} else {
				// res.send("test");
				if(req.files.uploadedVideo == undefined) {
					res.render("upload", {
						msg: "Error: No Video File Selected"
					});	
				}
				if(req.files.thumbnail == undefined) {
					res.render("upload", {
						msg: "Error: No Image File Selected"
					});	
				}

				let title = req.body.title;
				let description = req.body.description;
				let tags = req.body.tags;
				let category = req.body.category;

				const userCreator = getUser(req.session.user_id, function (result) {
					let currentTime = new Date().getTime();
					let videoNewPath = req.files.uploadedVideo[0]["path"];
					let imageNewPath = req.files.thumbnail[0]["path"];
					getVideoDurationInSeconds(videoNewPath)
						.then(async function(duration) {
							let hours = Math.floor(duration/60/60);
							let minutes = Math.floor(duration/60) - (hours * 60);
							let seconds = Math.floor(duration % 60);

							//Insert Video Data into database
							const video = await Video.create({
								"user": {
									"_id": result._id,
									"name": result.name,
									"image": result.image,
									"subscribers": result.subscribers,
								},
								"video": videoNewPath,
								"thumbnail": imageNewPath,
								"title": title,
								"description": description,
								"tags": tags,
								"category": category,
								"createdAt": currentTime,
								"minutes": minutes,
								"seconds": seconds,
								"hours": hours,
								"watch": currentTime,
								"views": 0,
								"playlist": "",
								"likers": [],
								"dislikers": [],

							}, async function (error, data) {
								//Insert in user collection as well
								const user = await User.updateOne({ id: req.session.user_id }, {
									$push: {
										"videos": {
											"_id": data._id,
											"title": title,
											"views": 0,
											"thumbnail": imageNewPath,
											"watch": currentTime
										}
									}
								}, {new: true, runValidators: true});

								res.redirect("/");
							})
						})
					});
				}
			})

	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const getVideo = (req, res) => {
	const watchId = req.params.id;
	const video = Video.findOne({ watch: watchId }, function(error, video) {
		if(video == null) {
			res.send("Video does not exists");
		}else{

			//increment views counter
			Video.updateOne({_id: video._id}, {$inc: {views: 1}}, function(err, incData) {
				res.render("video-page/index", {
					"isLogin": req.session.user_id ? true : false,
					"video": video,
					"user": {
						_id: req.session.user_id
					},
					"playlist": [],
					"playlistId": ""
				});
			})
		}
	})
}

const doLikes = async (req, res) => {
	if(req.session.user_id) {
		//check if already liked
		let videoId = req.body._id;
		Video.findOne({
			_id: videoId,
			"likers._id": req.session.user_id
		}, function (error, video) {
			console.log(video)
			if(video == null) {
				//push in likers array
				Video.updateOne({
					_id: videoId
				}, {
					$push: {
						"likers": {
							"_id":req.session.user_id
						}
					}
				}, function (error, data) {
					res.json({
						"status": "success",
						"message": "Video has been liked"
					});	
				})
			}else{
				res.json({
					"status": "error",
					"message": "Already Like this video"
				});
			}
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		});
	}
}

const doDislikes = async (req, res) => {
	if(req.session.user_id) {
		//check if already liked
		let videoId = req.body._id;
		console.log(videoId);
		Video.findOne({
			_id: videoId,
			"dislikers._id": req.session.user_id
		}, function (error, video) {
			console.log(video)
			if(video == null) {
				//push in likers array
				Video.updateOne({
					_id: videoId
				}, {
					$push: {
						"dislikers": {
							"_id":req.session.user_id
						}
					}
				}, function (error, data) {
					res.json({
						"status": "success",
						"message": "Video has been disliked"
					});	
				})
			}else{
				res.json({
					"status": "error",
					"message": "Already dislike this video"
				});
			}
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		});
	}
}

//Comment on a video
const doComments = async (req, res) => {
	if(req.session.user_id) {
		// console.log(req.body);
		let id = req.body._id;
		let commentBody = req.body.comment;
		getUser(req.session.user_id, function (user) {
			Video.findOneAndUpdate({
				_id: id 
			}, {
				$push: {
					"comments": {
						"_id": newId,
						"user": {
							"_id": user._id,
							"name": user.name,
							"image": user.image
						},
						"comment": commentBody,
						"createdAt": new Date().getTime(),
						"replies": []
					}
				}
			}, function (error, video) {
				//send notfications to video publisher
				console.log(video.user._id);
				// res.send(video)
				let channelId = video.user._id;
				User.updateOne({
					_id: channelId
				}, {
					$push: {
						"notifications": {
							"_id": newId,
							"notif_type": "new comment",
							"content": commentBody,
							"is_read": false,
							"video_watch": video.watch,
							user: {
								"_id": user._id,
								"name": user.name,
								"image": user.image
							}
						}
					}
				}, function (error, usersChannel) {
					res.json({
						"status": "success",
						"message": "Comment has been posted",
						user: {
							"_id": user._id,
							"name": user.name,
							"image": user.image
						}					
					})
				})
			})
		});
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		});
	}
}

const getNotifications = async (req, res) => {
	if(req.session.user_id) {
		getUser(req.session.user_id, function (user) {
			delete user.password;

			res.json({
				"status": "success",
				"message": "Record has been fetched",
				user: {
					"_id": user._id,
					"name": user.name,
					"image": user.image,
					"notifications": user.notifications
				}
			})
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		});
	}
}

const is_read_notif = (req, res) => {
	if(req.session.user_id) {
		let notif_id = req.body._id;
		const findUser = User.findOneAndUpdate({
			"notifications._id": notif_id
			// _id: mongoose.Types.ObjectId(req.session.user_id),
			// "notifications._id": mongoose.Types.ObjectId(notif_id)
		}, {
			$set: {
				"notifications.$.is_read": true
			}
		}, function (error, data) {
			res.json({
				"status": "success",
				"message": "Notification has been marked read"
			})
		});

		// console.log(findUser)
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}

const postReply = (req, res) => {
	if(req.session.user_id) {
		let reply = req.body.replyBody;
		let _id = req.body._id;
		getUser(req.session.user_id, function (user) {
			Video.findOneAndUpdate({
				"comments._id": _id
			}, {
				$push: {
					"comments.$.replies": {
						_id: newId,
						user: {
							"_id": user._id,
							"name": user.name,
							"image": user.image
						},
						reply: reply,
						createdAt: new Date().getTime()
					}
				}
			}, function (error, data) {
				let videoWatch = data.watch;
				for(let i = 0; i < data.comments.length; i++) {
					let comment = data.comments[i];
					console.log(comment);
					
					if(comment._id == _id) {
						let users_id = comment.user._id;
						User.updateOne({
							_id: users_id 
						}, {
							$push: {
								"notifications": {
									_id: newId,
									notif_type: "new_reply",
									content: reply,
									is_read: false,
									video_watch: videoWatch,
									user: {
										_id: user._id,
										name: user.name,
										image: user.image
									}
								}
							}
						}, function (error, userUpdate) {
							res.json({
								"status": "success",
								"message": "Reply has been posted",
								user: {
									_id: user._id,
									name: user.name,
									image: user.image
								}
							})
						})
					break;
					}
				}
			})
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}


const videoSubscribe = (req, res) => {
	if(req.session.user_id) {
		const videoId = req.body._id
		Video.findOne({
			_id: videoId
		}, function (error1, video) {
			if(req.session.user_id == video.user._id) {
				res.json({
					"status": "error",
					"message": "You cannot subscribe your own video"
				})
			}else{
				// Check if channel is already subscribed
				getUser(req.session.user_id, function (user) {
					let flag = false;
					for(let i = 0; i < user.subscriptions.length; i++) {
						const userData = user.subscriptions[i];
						if(userData._id.toString() == video.user._id.toString()) {
							flag = true;
							break;
						}
					}

					if(flag) {
						res.json({
							"status": "error",
							"message": "Already been subscribed"
						})
					}else{
						User.findOneAndUpdate({
							_id: video.user._id
						}, {
							$inc: { subscribers: 1 }
						}, {returnOriginal: false}, function (error2, userData) {
							User.updateOne({
								_id: req.session.user_id
							}, {
								$push: {
									subscriptions: {
										_id: video.user._id,
										name: video.user.name,
										subscribers: userData.subscribers,
										image: userData.image
									}
								}
							}, function (error3, data) {
								Video.findOneAndUpdate({
									_id: videoId
								}, {
									$inc: { "user.subscribers": 1 }
								}, function (error4, finalData) {
									res.json({
										"status": "success",
										"message": "Subscription has been added"
									})
								})
							})
						})
					}
				})
			}
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}

const getRelatedVideo = async (req, res) => {
	const videoId = req.query._id;
	Video.find({
		// "$ne": {
		// 	_id: req.query.videoId
		// }
		$and: [{
			"_id": {
				$ne: videoId
			}
		}, {
			"category": req.query.category
		}]
	}, function (error, data) {
		res.json({
			"status": "success",
			"message": "Video data has been fetched",
			"data": data
		})
	})
}

const saveHistory = async (req, res) => {
	const videoId = req.body.videoId;
	const watched = req.body.watchedTime;
	if(req.session.user_id) {
		Video.findOne({
			_id: videoId
		}, function (error, video) {
			User.findOne({
				$and: [{
					_id: req.session.user_id
				}, {
					"history.videoId": videoId
				}]
			}, function (error2, user) {
				//Push if history is null
				if(user === null) {
					User.updateOne({
						_id: req.session.user_id
					}, {
						$push: {
							"history": {
								_id: newId,
								videoId: videoId,
								watch: video.watch,
								title: video.title,
								watched: watched,
								thumbnail: video.thumbnail,
								minutes: video.minutes,
								seconds: video.seconds,

							}
						}
					}, function (error3, updatedUser) {
						res.json({
							"status": "success",
							"message": "History has been added"
						})
					})
				}else{
					User.updateOne({
						$and: [{
							_id: req.session.user_id
						}, {
							"history.videoId": videoId
						}]
					}, {
						$set: {
							"history.$.watched": watched
						}	
					}, function (error4, secondUpdate) {
						res.json({
							"status": "success",
							"message": "History has been updated"
						})
					})
				}
			})
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}

const getVideoHistory = (req, res) => {
	if(req.session.user_id) {
		getUser(req.session.user_id, function (user) {
			res.render("watch-history", {
				"isLogin": true,
				"videos": user.history
			});
		});
	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const deleteHistory = async (req, res) => {
	if(req.session.user_id) {
		User.updateOne({
			$and: [{
				_id: req.session.user_id
			}, {
				"history.videoId": req.body.historyId
			}]
		}, {
			$pull: {
				history: {
					videoId: req.body.historyId
				}
			}
		}, function (error, user) {
			res.json({
				"status": "success",
				"message": "History has been remove"
			});
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		});
	}
}

const changeProfilePic = (req, res) => {
	if(req.session.user_id) {
		upload(req, res, (err) => {
			if(err) {
				res.render("upload", {
					msg: err
				});
			}else{
				if(req.files.profilePicture == undefined) {
					res.render("upload", {
						msg: "Error: No Imag File Selected"
					});	
				}
				
				let imageNewPath = req.files.profilePicture[0]["path"];
				User.updateOne({
					_id: req.session.user_id
				}, {
					$set: {
						image: imageNewPath
					}
				}, function (error, user) {
					User.updateOne({
						"subscriptions._id": req.session.user_id
					}, {
						$set: {
							"subscriptions.$.image": imageNewPath
						}
					}, function (error2, userData) {
						Video.updateOne({
							"user._id": req.session.user_id
						}, {
							$set: {
								"user.image": imageNewPath
							}
						}, function (error3, finalData) {
							res.redirect("/api/v1/video/channel/" + req.session.user_id);
						})
					})
				})			
			}			
		})
	}else{
		res.redirect("/api/v1/auth/login")
	}
}


const ChangeCoverPic = (req, res) => {
	if(req.session.user_id) {
		upload(req, res, (err) => {
			if(err) {
				res.render("upload", {
					msg: err
				});
			}else{
				if(req.files.coverPicture == undefined) {
					res.render("upload", {
						msg: "Error: No Image File Selected"
					});	
				}
				
				let imageNewPath = req.files.coverPicture[0]["path"];
				User.updateOne({
					_id: req.session.user_id
				}, {
					$set: {
						coverPhoto: imageNewPath
					}
				}, function (error, user) {
					res.redirect("/api/v1/video/channel/" + req.session.user_id);
				})			
			}			
		})
	}else{
		res.redirect("/api/v1/auth/login")
	}
}

const renderEditPage = (req, res) => {
	if(req.session.user_id) {
		Video.findOne({
			$and: [{
				"watch": req.params.watch
			}, {
				"user._id": req.session.user_id
			}]
		}, function (error, video) {
			if(video == null) {
				res.json({
					"status": "error",
					"message": "Sorry you do not own this video"
				})
			}else{
				getUser(req.session.user_id, function (user) {
					res.render("edit-video", {
						"isLogin": true,
						"video": video,
						"user": user
					});
				})
			}
		})
	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const editVideo = async (req, res) => {
	if(req.session.user_id) {
		upload(req, res, (err) => {
			if(err) {
				res.render("upload", {
					msg: err
				});
			}else{
				if(req.files.thumbnail == undefined) {
					res.render("edit-video", {
						msg: "Error: No Image File Selected"
					});	
				}
				
				let imageNewPath = req.files.thumbnail[0]["path"];
				Video.findOne({
					$and: [{
						_id: req.body.videoId
					}, {
						"user._id": req.session.user_id
					}]
				}, function (error, mainVideo) {
					if(mainVideo == null) {
						res.json({
							"status": "error",
							"message": "Sorry you do not own this video"
						});
					}else{
						Video.findOneAndUpdate({
							_id: req.body.videoId
						}, {
							$set: {
								"title": req.body.title,
								"description": req.body.description,
								"tags": req.body.tags,
								"category": req.body.category,
								"thumbnail": mainVideo.thumbnail,
								"playlist": imageNewPath
							}
						}, function (error2, video) {
							if(req.body.playlists == "") {
								User.findOneAndUpdate({
									$and: [{
										_id: req.session.user_id
									}, {
										"videos._id": mongoose.Types.ObjectId(req.body.videoId)
									}]
								}, {
									$set: {
										"videos.$.title": req.body.title,
										"videos.$.thumbnail": imageNewPath
									}
								}, function (error3, data){
									User.updateOne({
										$and: [{
											_id: req.session.user_id
										}, {
											"playlists._id": mongoose.Types.ObjectId(mainVideo.playlist)
										}]
									}, {
										$pull: {
											"playlists.$.videos": {
												_id: req.body.videoId 
											}
										}
									}, function (userError, pushData) {
										res.redirect("/api/v1/video/edit/" + mainVideo.watch);
									});
								});
							}else{
								if(mainVideo.playlist != "") {
									User.updateOne({
										$and: [{
											_id: req.session.user_id
										}, {
											"playlists._id": mongoose.Types.ObjectId(mainVideo.playlist)
										}]
									}, {
										$pull: {
											"playlists.$.videos": {
												_id: req.body.videoId
											}
										}
									}, function (error4, userData) {
										User.updateOne({
											$and: [{
												_id: req.session.user_id
											}, {
												"playlists._id": mongoose.Types.ObjectId(req.body.playlists)
											}]
										}, {
											$push: {
												"playlists.$.videos": {
													_id: req.body.videoId,
													title: req.body.title,
													watch: mainVideo.watch,
													thumbnail: imageNewPath
												}
											}
										}, function (error5, finalData) {
											User.findOneAndUpdate({
												$and: [{
													_id: req.session.user_id
												}, {
													"videos._id": mongoose.Types.ObjectId(req.body.videoId)
												}]
											}, {
												$set: {
													"videos.$.title": req.body.title,
													"videos.$.thumbnail": imageNewPath
												}
											}, function (error6, userVideosData) {
												res.redirect("/api/v1/video/edit/" + mainVideo.watch);
											})
										});
									});
								}
							}
							
						});
					}
				});		
			}			
		});
	}else{
		res.redirect("/api/v1/auth/login")
	}	
}

const getUserId = (req, res) => {
	if(req.session.user_id) {
		getUser(req.session.user_id, function (user) {
			res.json({
				"status": "success",
				"message": "Success retrieing data",
				"userId": req.session.user_id
			});
		})
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}

const deleteVideo = async (req, res) => {
	console.log(req.body);
	console.log(req.params);
	if(req.session.user_id) {
		Video.findOne({
			$and: [{
				_id: req.body._id
			}, {
				"user._id": req.session.user_id
			}]
		}, function (error, video) {
			if(video == null) {
				res.json({
					"status": "error",
					"message": "Sorry, you do not own this video"
				})
			}else{
				fileSystem.unlink(video.video, function (error) {
					fileSystem.unlink(video.thumbnail, function (error) {

					});
				});
				Video.findOneAndRemove({
					$and: [{
						_id: req.body._id
					}, {
						"user._id": req.session.user_id
					}]
				}, function (error2, removedData) {
					User.findOneAndUpdate({
						_id: req.session.user_id
					}, {
						$pull: {
							"videos": {
								"_id": mongoose.Types.ObjectId(req.body._id)
							}
						}
					}, function (error3, usersUpdate) {
						User.updateMany({}, {
							$pull: {
								"history": {
									"videoId": req.body._id.toString()
								}
							}
						}, function (error4, finalData) {
							getUser(req.session.user_id, function (user) {
								let playlistId = "";
								for(let a = 0; a < user.playlists.length; a++) {
									for(let b = 0; b < user.playlists[a].videos.length; b++) {
										let video = user.playlists[a].videos[b];
										if(video._id == req.body._id) {
											playlistId = user.playlists[a]._id;
											break;
										}
									}
								}

								if(playlistId != "") {
									User.updateOne({
										$and: [{
											_id: req.session.user_id
										}, {
											"playlists._id": mongoose.Types.ObjectId(playlistId)
										}]
									}, {
										$pull: {
											"playlists.$.videos": {
												_id: req.body._id,

											}
										}
									});
								}
								res.redirect("/api/v1/video/channel/" + req.session.user_id);
							});
						})
					})
				})
			}
		})
	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const createPlaylist = async (req, res) => {
	if(req.session.user_id) {
		User.updateOne({
			_id:  mongoose.Types.ObjectId(req.session.user_id)
		}, {
			$push: {
				playlists: {
					_id: newId,
					playlistTitle: req.body.playlistTitle,
					videos: []
				}
			}
		}, function (error, user) {
			console.log(user);
			res.redirect("/api/v1/video/channel/" + req.session.user_id);
		})
	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const renderPlaylistPage = (req, res) => {
	Video.findOne({
		$and: [{
			"watch": parseInt(req.params.watch)
		}, {
			"playlist": req.params._id
		}]
	}, function (error, video) {
		if(video == null) {
			res.json({
				"status": "error",
				"message": "Video does not exist."
			})
		}else{
			Video.updateOne({
				_id: video._id
			}, {
				$inc: {
					"views": 1
				}
			}, function (error2, data) {
				getUser(video.user._id, function (user) {
					let playlistVideos = [];
					for(let i = 0; i < user.playlists.length; i++) {
						if(user.playlists[i]._id == req.params._id) {
							playlistVideos = user.playlists[i].videos;
							break;
						}
					}
					res.render("video-page/index", {
						"isLogin": req.session.user_id ? true : false,
						"video": video,
						"playlist": playlistVideos,
						"playlistId": req.params._id
					});
				});
			});
		}
	});
}

const deletePlaylist = async (req, res) => {
	if(req.session.user_id) {
		User.findOne({
			$and: [{
				_id: req.session.user_id
			}, {
				"playlist._id": mongoose.Types.ObjectId(req.body._id)
			}]
		}, function (error, user) {
			if(user == null) {
				res.json({
					"status": "error",
					"message": "Sorry. You do not own this playlist"
				})
			}else{
				User.updateOne({
					_id: req.session.user_id			
				}, {
					$pull: {
						"playlists": {
							_id: mongoose.Types.ObjectId(req.body._id)
						}
					}
				}, function (error2, removedUser) {
					Video.updateMany({
						playlist: req.body._id
					}, {
						$set: {
							"playlist": ""
						}
					}, function (error3, video) {
						res.redirect("/api/v1/video/channel/" + req.session.user_id);	
					});
				});
			}
		});
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}

const getSubscriptions = (req, res) => {
	if(req.session.user_id) {
		getUser(req.session.user_id, function (user) {
			res.render("subscriptions", {
				"isLogin": true,
				"user": user
			});
		});
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		});
	}
}

const unsubscribed = async (req, res) => {
	if(req.session.user_id) {
		User.updateOne({
			_id: req.session.user_id
		}, {
			$pull: {
				subscriptions: {
					_id: mongoose.Types.ObjectId(req.body._id)
				}
			}
		}, function (error, user) {
			if(user.modifiedCount > 0) {
				User.updateOne({
					_id: req.body._id
				}, {
					$inc: {
						subscribers: -1
					}
				}, function (error2, data) {
					Video.updateOne({
						"user._id": req.body._id
					}, {
						$inc: {
							"user.subscribers": -1
						}
					}, function (error3, unsubscribedData) {
						console.log(unsubscribedData);
						res.redirect("/api/v1/video/my-subscriptions");
					});
				});
			}
		});
	}else{
		res.redirect("/api/v1/auth/login");
	}
}

const categorySearch = async (req, res) => {
	Video.find({
		"category": {
			$regex: ".*?" + req.params.category + ".*?"		
		}
	}, function (error, videos) {
		// res.send(videos);
		res.render("search", {
			"isLogin": req.session.user_id ? true : false,
			"videos": videos,
			"query": req.params.category
		})
	});
}

const tagSearch = (req, res) => {
	Video.find({
		"tags": {
			$regex: ".*" + req.params.query + ".*",
			$options: "i"		
		}
	}, function (error, videos) {
		// res.send(videos);
		res.render("search", {
			"isLogin": req.session.user_id ? true : false,
			"videos": videos,
			"query": req.params.query
		})
	});	
}

const search = async (req, res) => {
	Video.find({
		"title": {
			$regex: req.query.search_query,
			$options: "i"		
		}
	}, function (error, videos) {
		// res.send(videos);
		res.render("search", {
			"isLogin": req.session.user_id ? true : false,
			"videos": videos,
			"query": req.query.search_query
		})
	});	
}

const settings = (req, res) => {
	if(req.session.user_id) {
		getUser(req.session.user_id, function (user) {
			res.render("settings", {
				"isLogin": true,
				"user": user,
				"request": req.query
			});
		});
	}else{
		res.json({
			"status": "error",
			"message": "Please login"
		})
	}
}

const testRoute = async (req, res) => {
	// const user = await User.findOne(req.session.user_id);
	// const videos = await Video.find({}).sort({"createdAt": -1}).toArray(function (err, videos) {
	// 	res.json({videos});	
	// })

	// const videos = await Video.find({});
	// const videos = Video.updateOne({_id: {"61c2de3be376222f1cdf4c80"}}, {$inc: {views: 1}})
	res.send("HEllow")
	// res.send(user);
}

module.exports = {
	uploadVideos,
	createVideo,
	testRoute,
	getVideo,
	doLikes,
 	doDislikes,
 	doComments,
 	getNotifications,
 	is_read_notif,
 	postReply,
 	videoSubscribe,
 	getRelatedVideo,
 	saveHistory,
 	getVideoHistory,
 	deleteHistory,
 	usersChannel,
 	changeProfilePic,
 	ChangeCoverPic,
 	renderEditPage,
 	editVideo,
 	getUserId,
 	deleteVideo,
 	createPlaylist,
 	renderPlaylistPage,
 	deletePlaylist,
 	getSubscriptions,
 	unsubscribed,
 	categorySearch,
 	tagSearch,
 	search,
 	settings
}