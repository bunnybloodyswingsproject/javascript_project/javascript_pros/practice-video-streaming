const express = require("express");
const router = express.Router();
const {
	renderRegisterPage,
	createUser,
	renderLogin,
	userLogin,
	logout
} = require("../controllers/auth");


// router.get("/signup", register);
router.route("/signup").get(renderRegisterPage).post(createUser);
// router.get("/login", renderlogin);
router.route("/login").get(renderLogin).post(userLogin);
router.get("/logout", logout);

module.exports = router;
