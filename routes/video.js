const express = require("express");
const router = express.Router();
const app = express();
const session = require("express-session");
const path = require("path");
const multer = require("multer");

// //define storage for the images
// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, './public/videos')
//   },
//   filename: function (req, file, cb) {
//   	let extArray = file.mimetype.split("/");
//     let extension = extArray[extArray.length - 1];
//     const uniqueSuffix = Date.now() + '-' + file.originalname + "." + extension;
//     cb(null, file.fieldname + '-' + uniqueSuffix)
//   }
// })

// const upload = multer({ 
// 	storage: storage,
// 	limits: {fileSize: 1048576000},
// 	fileFilter: function(req, file, cb) {
// 		checkFileType(file, cb);
// 	}
// }).single("uploadedVideo");


//define storage for the images/videos
// const storage = multer.diskStorage({
//   //Setting destination of uploading files	
//   destination: function (req, file, cb) {
//   	//if uploading videos
//   	if(file.fieldname === "uploadedVideo") {
//   		cb(null, './public/videos');
//   	}else{
//   		cb(null, "./public/thumbnails");
//   	}
//   },
//   filename: function (req, file, cb) {
//     const uniqueSuffix = Date.now() + '-' + file.originalname;
//     cb(null, file.fieldname + '-' + uniqueSuffix)
//   }
// })

// const upload = multer({ 
// 	storage: storage,
// 	limits: {fileSize: function(req, file, cb) {
// 		checkFileSize(file, cb);
// 	}},
// 	fileFilter: function(req, file, cb) {
// 		checkFileType(file, cb);
// 	}
// }).fields([
// 	{
// 		name: "uploadedVideo",
// 		maxCount: 1
// 	},
// 	{
// 		name: "thumbnail",
// 		maxCount: 1
// 	}
// ]);

// //Check files filesize
// function checkFileSize(file, cb) {
// 	if(file.fieldname === "uploadedVideo") {
// 		return 1048576000;
// 	}else{
// 		return 10000000;
// 	}
// }

// Check file type
// function checkFileType(file, cb) {
// 	if(file.fieldname === "uploadedVideo") {
// 		//Allowed extension
// 		const filetypes = /mp4|gif/;
// 		//Check ext
// 		const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
// 		//Check mime
// 		const mimetype = filetypes.test(file.mimetype);

// 		if(mimetype && extname) {
// 			return cb(null, true);
// 		}else{
// 			return cb("Error: Video field should be images files Only!")
// 		}		
// 	}else{
// 		//Allowed extension
// 		const filetypes = /jpeg|jpg|png|gif/;
// 		//Check ext
// 		const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
// 		//Check mime
// 		const mimetype = filetypes.test(file.mimetype);

// 		if(mimetype && extname) {
// 			return cb(null, true);
// 		}else{
// 			return cb("Error: Thumbnail field should be images files Only!")
// 		}
// 	}
// }

const {
	uploadVideos,
	createVideo,
	testRoute,
	getVideo,
	doLikes,
	doDislikes,
	doComments,
	getNotifications,
	is_read_notif,
	postReply,
	videoSubscribe,
	getRelatedVideo,
	saveHistory,
	getVideoHistory,
	deleteHistory,
	usersChannel,
	changeProfilePic,
	ChangeCoverPic,
	renderEditPage,
	editVideo,
	getUserId,
	deleteVideo,
	createPlaylist,
	renderPlaylistPage,
	deletePlaylist,
	getSubscriptions,
	unsubscribed,
	categorySearch,
	tagSearch,
	search,
	settings
} = require("../controllers/video");

router.route("/get-user-id").get(getUserId);
router.route("/upload-video").get(uploadVideos);
router.post("/upload-video", createVideo);
router.route("/watch/:id").get(getVideo);
router.patch("/do-likes/:videoId", doLikes);
router.patch("/do-disLikes/:videoId", doDislikes);
router.post("/do-comment/:videoId", doComments);
router.route("/get-notif").get(getNotifications).patch(is_read_notif);
router.route("/do-comment").post(postReply);
router.post("/do-subscribe", videoSubscribe);
router.get("/get-related-videos", getRelatedVideo);
router.post("/save-history", saveHistory);
router.get("/watch-history", getVideoHistory);
router.post("/delete-history", deleteHistory);
router.get("/channel/:user_id", usersChannel);
router.post("/change-profile-picture", changeProfilePic);
router.post("/change-cover-picture", ChangeCoverPic);
router.get("/edit/:watch", renderEditPage);
router.post("/edit", editVideo);
router.post("/delete-video", deleteVideo);
router.post("/create-playlist", createPlaylist);
router.get("/playlist/:_id/:watch", renderPlaylistPage);
router.post("/delete-playlist", deletePlaylist);
router.get("/my-subscriptions", getSubscriptions);
router.post("/remove-channel-from-subscription", unsubscribed)
router.get("/category_search/:category", categorySearch)
router.get("/tag_search/:query", tagSearch);
router.get("/search/", search);
router.get("/settings", settings);

//test route
router.get("/test", testRoute)
module.exports = router;
