const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const UserSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Please provide name"],
		maxlength: 50,
		minlength: 3
	},
	email: {
		type: String,
		required: [true, "Please provide email"],
		match: [
			/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			"Please providea valid email"
		],
		unique: true
	},
	password: {
		type: String,
		required: [true, "Please provide password"],
		minlength: 6
	},
	coverPhoto: {
		type: String,
		default: ""
	},
	image: {
		type: String,
		default: ""
	},
	subscribers: {
		type: Number,
		default: 0
	},
	history: Array,
	subscriptions: {
		type: Array,
		default: []
	},
	playlists: {
		type: Array,
		default: []
	},
	videos: {
		type: Array,
		default: []
	},
	notifications: [{
		_id: mongoose.Types.ObjectId,
		notif_type: {
			type: String,
			default: "new comment",
		},
		content: String,
		is_read: {
			type: Boolean,
			default: false
		},
		video_watch: Number,
		user: {
			_id: mongoose.Types.ObjectId,
			name: String,
			image: String
		}
	}],
});

UserSchema.pre("save", async function () {
	const salt = await bcrypt.genSalt(10);
	this.password = await bcrypt.hash(this.password, salt);
})

UserSchema.methods.createJWT = function () {
	return jwt.sign(
		{
			userId: this._id, name: this.name
		}, process.env.JWT_SECRET,
		{
			expiresIn: process.env.JWT_LIFETIME
		}
	)
}

UserSchema.methods.comparePassword = async function(candidatePassword) {
	const isMatch = await bcrypt.compare(candidatePassword, this.password);
	return isMatch;
}

module.exports = mongoose.model("User", UserSchema);
