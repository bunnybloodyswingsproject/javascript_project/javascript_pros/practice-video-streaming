const mongoose = require("mongoose");

const VideoSchema = new mongoose.Schema({
	title: {
		type: String,
		required: [true, "Please provide title"],
		maxlength: 50,
		minlength: 3
	},
	description: {
		type: String,
		required: [true, "Please provide email"]
	},
	tags: String,
	category: {
		type: String,
		enum: ["Technology", "Gaming", "Educaion", "Funny",]
	},
	video: String,
	thumbnail: String,
	hours: Number,
	minutes: Number,
	seconds: Number,
	createdAt: Number,
	watch: Number,
	views: {
		type: Number,
		default: 0
	},
	playlist: String,
	likers: Array,
	dislikers: Array,
	comments: [{
			_id: mongoose.Types.ObjectId,
			user: {
				_id: mongoose.Types.ObjectId,
				name: String,
				image: String
			},
			comment: String,
			createdAt: Date,
			replies: Array
		}],
	user: {
		_id: mongoose.Types.ObjectId,
		name: String,
		image: String,
		subscribers: {
			type: Number,
			default: 0
		}
	}
}, {timestamps: true});


module.exports = mongoose.model("Video", VideoSchema);
