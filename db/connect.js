const mongoose = require("mongoose");

const connectDB = (url) => {
	return mongoose.connect(url, {
		useNewUrlParser: true,
		// useCreateIndex: true, => no longer needed as per mongoose 6.0
		// useFindAndModify: false, => no longer needed as per mongoose 6.0
		useUnifiedTopology: true,
	})
}

module.exports = connectDB;